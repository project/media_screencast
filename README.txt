
Media: Screencast

This adds support for the Screencast video sharing service, available at http://www.screencast.com/

To use this module, you'll first need to install Embedded Video Field, which is
packaged with Embedded Media Field (from http://drupal.org/project/emfield).

Set up a content type to use a Third Party Video field as you normally would with emfield.
Also ensure that you have enabled the new Screencast provider from the Admin screen at /admin/content/emfield.

