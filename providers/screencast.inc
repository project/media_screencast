<?php


define('EMVIDEO_SCREENCAST_MAIN_URL', 'http://www.screencast.com/');
define('EMVIDEO_SCREENCAST_COLOR_DEFAULT', '#FFFFFF');

/**
 * Hook emvideo_PROVIDER_info
 */
function emvideo_screencast_info() {
  $name = t('Screencast.com');

  return array(
    'provider' => 'screencast',
    'name' => $name,
    'url' => EMVIDEO_SCREENCAST_MAIN_URL,
    'settings_description' => t('These settings specifically affect videos displayed from !provider.', array('!provider' => l($name, EMVIDEO_SCREENCAST_MAIN_URL, array('target' => '_blank')))),
    'supported_features' => array(
      array(t('Autoplay'), t('No'), ''),
      array(t('Full screen mode'), t('Yes'), t('You may customize the player to enable or disable full screen playback. Full screen mode is enabled by default.')),
      array(t('Thumbnails'), t('Yes'), ''),
      array(t('Custom player color'), t('Yes'), t('You may customize the player\'s skin by choosing your own color.'))
    )
  );  
}

/**
 * Hook emvideo_PROVIDER_settings
 */
function emvideo_screencast_settings() {
  $form = array();

  $form['screencast']['emvideo_screencast_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Background color'),
    '#default_value' => variable_get('emvideo_screencast_color', EMVIDEO_SCREENCAST_COLOR_DEFAULT),
  );
  $form['screencast']['emvideo_screencast_full_screen'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow fullscreen'),
    '#default_value' => variable_get('emvideo_screencast_full_screen', 1),
    '#description' => t('Allow users to view video using the entire computer screen.'),
  );

  if (module_exists('colorpicker')) {
    $form['screencast']['emvideo_screencast_color']['#type'] = 'colorpicker_textfield';
    $form['screencast']['emvideo_screencast_color']['#colorpicker'] = 'colorpicker_1';
    $form['screencast']['colorpicker_1'] = array(
      '#type' => 'colorpicker',
      '#title' => t('Color picker'),
      '#description' => t('Click in this textfield to start picking your color')
    );
  }

  return $form;
}

/**
 * Hook emvideo_PROVIDER_extract
 */
function emvideo_screencast_extract($embed = '') {
  return array(
    '@content=http://content\.screencast\.com/users/([^"]+)"@i'
  );
}

/**
 * Hook emvideo_PROVIDER_embedded_link($video_code)
 */
function emvideo_screencast_embedded_link($video_code) {
  return 'http://www.screencast.com/users/'. _emvideo_screencast_drop_file($video_code);
}

function theme_emvideo_screencast_flash($embed, $width, $height, $autoplay) {
  static $count = 0;

  $out = '';

  if ($embed) {
    $embed_no_file = _emvideo_screencast_drop_file($embed);
    $color = variable_get('emvideo_screencast_color', EMVIDEO_SCREENCAST_COLOR_DEFAULT);
    $full_screen = variable_get('emvideo_screencast_full_screen', 1) ? 'true' : 'false';
    $id = 'video-cck-screencast-flash-wrapper-'. (++$count);

    $out .= <<<FLASH
<div id="$id">
<object width="$width" height="$height">
  <param name="movie" value="http://content.screencast.com/users/$embed_no_file/flvplayer.swf"></param>
  <param name="quality" value="high"></param>
  <param name="bgcolor" value="$color"></param>
  <param name="flashVars" value="thumb=http://content.screencast.com/users/$embed_no_file/FirstFrame.jpg&width=$width&height=$height&content=http://content.screencast.com/users/$embed"></param>
  <param name="allowFullScreen" value="$full_screen"></param>
  <param name="scale" value="showall"></param>
  <param name="allowScriptAccess" value="always"></param>
  <param name="base" value="http://content.screencast.com/users/$embed_no_file/"></param>
  <embed src="http://content.screencast.com/users/$embed_no_file/flvplayer.swf" quality="high" bgcolor="$color" width="$width" height="$height" type="application/x-shockwave-flash" allowScriptAccess="always" flashVars="thumb=http://content.screencast.com/users/$embed_no_file/FirstFrame.jpg&width=$width&height=$height&content=http://content.screencast.com/users/$embed" allowFullScreen="$full_screen" base="http://content.screencast.com/users/$embed_no_file/" scale="showall"></embed>
</object>
</div>
FLASH;
  }

  return $out;
}

/**
 * Hook emvideo_PROVIDER_thumbnail
 */
function emvideo_screencast_thumbnail($field, $item, $formatter, $node, $width, $height) {
  return 'http://content.screencast.com/users/'. _emvideo_screencast_drop_file($item['value']) .'/FirstFrame.jpg';
}

/**
 * Hook emvideo_PROVIDER_video
 */
function emvideo_screencast_video($embed, $width, $height, $field, $item, &$node, $autoplay) {
  $output = theme('emvideo_screencast_flash', $embed, $width, $height, $autoplay);
  return $output;
}

/**
 * Hook emvideo_PROVIDER_preview
 */
function emvideo_screencast_preview($embed, $width, $height, $field, $item, &$node, $autoplay) {
  $output = theme('emvideo_screencast_flash', $embed, $width, $height, $autoplay);
  return $output;
}

function _emvideo_screencast_drop_file($video_code) {
  $file = strrchr($video_code, '/');

  return substr($video_code, 0, - strlen($file));
}

/**
 * Implementation of hook_emfield_subtheme.
 */
function emvideo_screencast_emfield_subtheme() {
  $themes = array(
    'emvideo_screencast_flash'  => array(
      'arguments' => array('embed' => NULL, 'width' => NULL, 'height' => NULL, 'autoplay' => NULL),
      'file' => 'providers/screencast.inc',
      'path' => drupal_get_path('module', 'media_screencast'),
    )
  );

  return $themes;
}

